""" Space Assembler """

from struct import pack
from sections import all_sections_cls



class SpaceAssembler:

	@classmethod
	def assembly(cls, bin_path, sections):
		all_data = b''
		sec_num = len(sections)
		offset = (sec_num+1)*24
		new_sections = []

		bwtb = pack(
			'4s5I',
			b'BWTB',
			1,
			offset,
			0,
			0,
			sec_num
		)

		for cls in all_sections_cls:
			if cls.header not in sections:
				continue
			new_sections.append({
				'header': cls.header.encode('ascii'),
				'int1': cls.int1,
				'data': sections.pop(cls.header).to_bin()
			})

		assert not sections

		for item in new_sections:
			data = item['data']
			all_data += data
			bwtb += pack(
				'4s5I',
				item['header'],
				item['int1'],
				offset,
				0,
				len(data),
				0
			)
			offset += len(data)

		with open(bin_path, 'wb') as out:
			out.write(bwtb)
			out.write(all_data)
