﻿import json
import os

unpacker_version = '0.1.3'

from sections import all_sections_cls
from space_assembler import SpaceAssembler



class CompiledSpace:
	sections = None

	def from_dir(self, unp_dir):
		with open(os.path.join(unp_dir, 'info.json'), 'r') as fr:
			info = json.load(fr)
		assert info['unpacker_version'] == unpacker_version

		self.sections = {}
		for sec_cls in all_sections_cls:
			sec = sec_cls(unp_dir)
			if sec._exist:
				self.sections[sec_cls.header] = sec

	def save_to_bin(self, bin_path):
		SpaceAssembler.assembly(bin_path, self.sections)
