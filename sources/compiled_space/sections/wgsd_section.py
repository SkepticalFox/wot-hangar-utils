""" WGSD (Decal) """

from struct import pack
from _base_json_section import *



def packDecal1Info(item):
	return pack(
		'<I16f4I4b6fI',
		item['accurate'],
		item['transform'][0],
		item['transform'][1],
		item['transform'][2],
		item['transform'][3],
		item['transform'][4],
		item['transform'][5],
		item['transform'][6],
		item['transform'][7],
		item['transform'][8],
		item['transform'][9],
		item['transform'][10],
		item['transform'][11],
		item['transform'][12],
		item['transform'][13],
		item['transform'][14],
		item['transform'][15],
		item['diff_tex_id'],
		item['bump_tex_id'],
		item['hm_tex_id'],
		item['unknown_1'],
		item['priority'],
		item['influence'],
		item['type'],
		item['unknown_2'],
		item['offsets'][0],
		item['offsets'][1],
		item['offsets'][2],
		item['offsets'][3],
		item['uv_wrapping'][0],
		item['uv_wrapping'][1],
		item['visibility_mask']
	)



def packDecal2Info(item):
	return pack(
		'<6f2I',
		item['floats'][0],
		item['floats'][1],
		item['floats'][2],
		item['floats'][3],
		item['floats'][4],
		item['floats'][5],
		item['unknown_1'],
		item['unknown_2']
	)



class WGSD_Section(Base_JSON_Section):
	header = 'WGSD'
	int1 = 2

	def to_bin(self):
		res = self.write_entries(self._data['1'], 116, packDecal1Info)
		res += self.write_entries(self._data['2'], 32, packDecal2Info)
		return res
