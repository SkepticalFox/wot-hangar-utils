""" WTbl (?) """

from struct import pack
from _base_json_section import *



class WTbl_Section(Base_JSON_Section):
	header = 'WTbl'
	int1 = 0

	def to_bin(self):
		return self.write_entries(self._data, 12, '<3I')
