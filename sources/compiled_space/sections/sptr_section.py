﻿""" SpTr (SpeedTree) """

from struct import pack
from _base_json_section import *



def packSpTrInfo(item):
	flags = 1 if item['casts_shadow'] else 0
	flags |= 2 if item['reflection_visible'] else 0
	flags |= 4 if item['casts_local_shadow'] else 0
	flags |= 8 if item['editor_only_casts_shadow'] else 0
	return pack(
		'<16f4I',
		item['transform'][0],
		item['transform'][1],
		item['transform'][2],
		item['transform'][3],
		item['transform'][4],
		item['transform'][5],
		item['transform'][6],
		item['transform'][7],
		item['transform'][8],
		item['transform'][9],
		item['transform'][10],
		item['transform'][11],
		item['transform'][12],
		item['transform'][13],
		item['transform'][14],
		item['transform'][15],
		item['spt_id'],
		item['seed'],
		flags,
		item['visibility_mask']
	)



class SpTr_Section(Base_JSON_Section):
	header = 'SpTr'
	int1 = 3

	def to_bin(self):
		res = self.write_entries(self._data['speedtree_list'], 80, packSpTrInfo)
		res += pack('<I6f', 24, *self._data['info'])
		return res
