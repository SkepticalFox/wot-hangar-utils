import os

from _base_section import *



class Base_XML_Section(Base_Section):
	def from_dir(self, unp_dir):
		try:
			self._data = open(os.path.join(unp_dir, '%s.xml' % self.header), 'rb').read()
			self._exist = True
		except:
			self._exist = False

	def to_bin(self):
		return self._data
