""" WGDE (?) """

from _base_json_section import *



class WGDE_Section(Base_JSON_Section):
	header = 'WGDE'
	int1 = 1

	def to_bin(self):
		res = self.write_entries(self._data['1_12'], 12, '<3I')
		res += self.write_entries(self._data['2_8'], 8, '<2I')
		res += self.write_entries(self._data['3_4'], 4, '<I')
		return res
