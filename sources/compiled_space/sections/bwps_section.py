""" BWPs (Particles) """

from struct import pack
from _base_json_section import *



def packParticleInfo(item):
	return pack(
		'<16f2If',
		item['transform'][0],
		item['transform'][1],
		item['transform'][2],
		item['transform'][3],
		item['transform'][4],
		item['transform'][5],
		item['transform'][6],
		item['transform'][7],
		item['transform'][8],
		item['transform'][9],
		item['transform'][10],
		item['transform'][11],
		item['transform'][12],
		item['transform'][13],
		item['transform'][14],
		item['transform'][15],
		item['resource_id'],
		packBools(item['reflection_visible'], False, False, False),
		item['seed_time']
	)



class BWPs_Section(Base_JSON_Section):
	header = 'BWPs'
	int1 = 1

	def to_bin(self):
		return self.write_entries(self._data, 76, packParticleInfo)
