""" BSGD (?) """

from _base_binary_section import *



class BSGD_Section(Base_Binary_Section):
	header = 'BSGD'
	int1 = 2

	def to_bin(self):
		return self._data
