﻿""" BWT2 (Terrain 2) """

from struct import pack
from _base_json_section import *



def packList1Item(item):
	return pack('<2I', item['resource_id'], item['loc'])



class BWT2_Section(Base_JSON_Section):
	header = 'BWT2'
	int1 = 2

	def to_bin(self):
		d0 = self._data['00']
		res = pack(
			'<If4i3I',
			32,
			d0['chunk_size'],
			d0['bounds']['min_1'],
			d0['bounds']['max_1'],
			d0['bounds']['min_2'],
			d0['bounds']['max_2'],
			d0['normal_map_id'],
			d0['unknown_1'],
			d0['unknown_2']
		)
		res += self.write_entries(self._data['01'], 8, packList1Item)
		res += self.write_entries(self._data['02'], 4, '<i')
		res += pack('<11I17fI9f', 148, *self._data['03_all'])
		res += self.write_entries(self._data['04_lod_distances'], 4, '<f')
		res += self.write_entries(self._data['05'], 8, '<2I')
		res += pack('<2I4fI', 24, *self._data['06'])
		res += self.write_entries(self._data['07'], 24, '<6f')
		res += self.write_entries(self._data['08'], 12, '<3f')
		res += self.write_entries(self._data['09'], 16, '<4I')
		res += self.write_entries(self._data['10'], 4, '<i')
		res += self.write_entries(self._data['11'], 2, '<h')
		res += self.write_entries(self._data['12'], 4, '<I')
		res += self.write_entries(self._data['13'], 4, '<I')
		return res
