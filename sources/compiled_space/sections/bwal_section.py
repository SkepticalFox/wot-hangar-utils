""" BWAL (Asset List) """

from struct import pack
from _base_json_section import *



def packAssetInfo(item):
	return pack(
		'<2I',
		item['asset_type'],
		item['string_id']
	)



class BWAL_Section(Base_JSON_Section):
	header = 'BWAL'
	int1 = 2

	def to_bin(self):
		return self.write_entries(self._data, 8, packAssetInfo)
