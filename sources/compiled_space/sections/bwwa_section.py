""" BWWa (Water) """

from struct import pack
from _base_json_section import *



def packWaterInfo(item):
	return pack(
		'<34f3I17fI4f8I',
		item['position'][0],
		item['position'][1],
		item['position'][2],
		item['size'][0],
		item['size'][1],
		item['orientation'],
		item['tessellation'],
		item['texture_tessellation'],
		item['consistency'],
		item['reflection_strength'],
		item['refraction_strength'],
		item['water_contrast'],
		item['animation_speed'],
		item['scroll_speed1'][0],
		item['scroll_speed1'][1],
		item['scroll_speed2'][0],
		item['scroll_speed2'][1],
		item['wave_scale'][0],
		item['wave_scale'][1],
		item['sun_power'],
		item['sun_scale'],
		item['sun_scale_deferred'],
		item['wind_velocity'],
		item['depth'],
		item['reflection_tint'][0],
		item['reflection_tint'][1],
		item['refraction_tint'][0],
		item['refraction_tint'][1],
		item['cellsize'],
		item['smoothness'],
		packBools(item['use_edge_alpha'], item['use_simulation'], False, False),
		item['35_unknown'],
		packBools(item['reflect_bottom'], False, False, False),
		item['deep_water_opacity_forward'],
		item['bank_distance_opacity_forward'],
		item['opacity_multiplier_forward'],
		item['opacity_power_forward'],
		item['shallow_water_bias_forward'],
		item['deep_colour'][0],
		item['deep_colour'][1],
		item['deep_colour'][2],
		item['deep_colour_multiplier'],
		item['fade_depth'],
		item['foam_intersection'],
		item['foam_multiplier'],
		item['foam_tiling'],
		item['foam_freq'],
		item['foam_amplitude'],
		item['foam_width'],
		packBools(item['bypass_depth'], False, False, False),
		item['freq_x'],
		item['freq_z'],
		item['wave_height'],
		item['caustics_power'],
		item['wave_texture_number'],
		packBools(item['use_shadows'], item['use_water_probes'], False, False),
		item['foam_texture_id'],
		item['wave_texture_id'],
		item['reflection_texture_id'],
		item['odata_path_id'],
		item['65_unknown'],
		item['66_unknown']
	)



class BWWa_Section(Base_JSON_Section):
	header = 'BWWa'
	int1 = 2

	def to_bin(self):
		res = self.write_entries(self._data['1'], 268, packWaterInfo)
		res += self.write_entries(self._data['2'], 24, '<6I')
		return res
