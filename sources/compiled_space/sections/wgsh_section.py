""" WGSH (?) """

from struct import pack
from _base_json_section import *



class WGSH_Section(Base_JSON_Section):
	header = 'WGSH'
	int1 = 1

	def to_bin(self):
		return self.write_entries(self._data, 44, '<11I')
