﻿""" BSMA (Materials) """

from struct import pack
from _base_json_section import *



def packMaterialInfo(item):
	return pack(
		'<3i',
		item['key_fx'],
		item['key_from'],
		item['key_to']
	)



def packPropertyInfo(item):
	return pack(
		'<2If' if item['value_type'] == 2 else '<3I',
		item['key'],
		item['value_type'],
		item['value']
	)



class BSMA_Section(Base_JSON_Section):
	header = 'BSMA'
	int1 = 1

	def to_bin(self):
		res = self.write_entries(self._data['materials'], 12, packMaterialInfo)
		res += self.write_entries(self._data['fx'], 4, '<I')
		res += self.write_entries(self._data['props'], 12, packPropertyInfo)
		res += self.write_entries(self._data['matrices'], 64, '<16f')
		def pack_vectors_item(item):
			return pack(
				'<4f',
				item[0],
				item[1],
				item[2],
				item[3]
			)
		res += self.write_entries(self._data['vectors'], 16, pack_vectors_item)
		return res
