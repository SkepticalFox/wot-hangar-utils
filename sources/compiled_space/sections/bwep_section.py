""" BWEP (?) """

from struct import pack
from _base_json_section import *



class BWEP_Section(Base_JSON_Section):
	header = 'BWEP'
	int1 = 4

	def to_bin(self):
		res = self.write_entries(self._data['1'], 172, '<43I')
		res += self.write_entries(self._data['2'], 172, '<43I')
		return res
