﻿""" BSMI (Model Instances) """

from _base_json_section import *



class BSMI_Section(Base_JSON_Section):
	header = 'BSMI'
	int1 = 2

	def to_bin(self):
		res = self.write_entries(self._data['01_64'], 64, '<16f')
		def pack_02_08_item(item):
			return pack(
				'<2I',
				item['0'] \
					| (int(item['casts_shadow'])					<< 0)
					| (int(item['reflection_visible'])				<< 1)
					| (int(item['shadow_proxy'])					<< 3)
					| (int(item['casts_local_shadow'])				<< 4)
					| (int(item['not_ignores_objects_farplane'])	<< 5)
					| (int(item['has_animations'])					<< 6),
				item['1']
			)
		res += self.write_entries(self._data['02_08'], 8, pack_02_08_item)
		res += self.write_entries(self._data['03_04'], 4, '<I')
		res += self.write_entries(self._data['04_04'], 4, '<I')
		res += self.write_entries(self._data['05_04'], 4, '<I')
		def pack_06_16_item(item):
			return pack(
				'<IiIf',
				item['unknown_1'],
				item['animation_id'],
				item['unknown_2'],
				item['frame_rate_multiplier']
			)
		res += self.write_entries(self._data['06_16'], 16, pack_06_16_item)
		res += self.write_entries(self._data['07_04'], 4, '<I')
		res += self.write_entries(self._data['08_12'], 12, '<3I')
		res += self.write_entries(self._data['09_04'], 4, '<I')
		res += self.write_entries(self._data['10_20'], 20, '<5f')
		return res
