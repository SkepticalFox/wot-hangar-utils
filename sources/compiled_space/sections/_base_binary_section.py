import os

from _base_section import *



class Base_Binary_Section(Base_Section):
	def from_dir(self, unp_dir):
		binary_file = os.path.join(unp_dir, '%s.bin' % self.header)
		self._exist = os.path.exists(binary_file)
		if not self._exist:
			return
		with open(binary_file, 'rb') as fr:
			self._data = fr.read()
