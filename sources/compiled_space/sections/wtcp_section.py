""" WTCP (WoT static scene Control Point) """

from struct import pack
from _base_json_section import *



class WTCP_Section(Base_JSON_Section):
	header = 'WTCP'
	int1 = 2

	def to_bin(self):
		def packWTCPInfo(item):
			return pack(
				'<17f2I5f3If3I',
				item['transform'][0],
				item['transform'][1],
				item['transform'][2],
				item['transform'][3],
				item['transform'][4],
				item['transform'][5],
				item['transform'][6],
				item['transform'][7],
				item['transform'][8],
				item['transform'][9],
				item['transform'][10],
				item['transform'][11],
				item['transform'][12],
				item['transform'][13],
				item['transform'][14],
				item['transform'][15],
				item['radius'],
				item['team'],
				item['base_id'],
				item['over_terrain_height'],
				item['radius_color'][0],
				item['radius_color'][1],
				item['radius_color'][2],
				item['radius_color'][3],
				item['flag_path_id'],
				item['flagstaff_path_id'],
				item['radius_path_id'],
				item['flag_scale'],
				item['event_name_id'],
				packBools(item['before_wind'], False, False, False),
				item['visibility_mask']
			)
		return self.write_entries(self._data, 124, packWTCPInfo)
