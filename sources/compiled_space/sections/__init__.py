﻿from bwst_section import BWST_Section
from bsma_section import BSMA_Section
from wtau_section import WTau_Section
from bwal_section import BWAL_Section
from bwlc_section import BWLC_Section
from sptr_section import SpTr_Section
from bwps_section import BWPs_Section
from bsmi_section import BSMI_Section
from bwt2_section import BWT2_Section
from bsmo_section import BSMO_Section
from bwcs_section import BWCS_Section
from bwfr_section import BWfr_Section
from wgsd_section import WGSD_Section
from wtcp_section import WTCP_Section
from udos_section import UDOS_Section
from cent_section import CENT_Section
from bwwa_section import BWWa_Section
from bwsg_section import BWSG_Section
from wgde_section import WGDE_Section
from bsgd_section import BSGD_Section
from bwep_section import BWEP_Section
from wtbl_section import WTbl_Section
from wgsh_section import WGSH_Section



all_sections_cls = [
	BWST_Section,
	BWAL_Section,
	BWCS_Section,
	BWSG_Section,
	BSGD_Section,
	BWT2_Section,
	BSMI_Section,
	BSMO_Section,
	BSMA_Section,
	SpTr_Section,
	BWfr_Section,
	WGSD_Section,
	WTCP_Section,
	BWWa_Section,
	BWEP_Section,
	BWPs_Section,
	CENT_Section,
	UDOS_Section,
	WGDE_Section,
	BWLC_Section,
	WTau_Section,
	WTbl_Section,
	WGSH_Section
]
