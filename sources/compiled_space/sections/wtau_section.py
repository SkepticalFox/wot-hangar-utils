""" WTau (Audio) """

from struct import pack
from _base_json_section import *



def packAudio1Info(item):
	return pack(
		'<2I4f', 
		item['wwevent_name_id'],
		item['event_name_id'],
		item['max_distance'],
		item['transform'][0],
		item['transform'][1],
		item['transform'][2]
	)



class WTau_Section(Base_JSON_Section):
	header = 'WTau'
	int1 = 2

	def to_bin(self):
		res = self.write_entries(self._data['1'], 24, packAudio1Info)
		res += self.write_entries(self._data['2'], 24, '<6I')
		return res
