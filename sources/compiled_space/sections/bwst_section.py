﻿""" BWST (String Table) """

from struct import pack
from _base_json_section import *



class BWST_Section(Base_JSON_Section):
	header = 'BWST'
	int1 = 2

	def to_bin(self):
		res = pack('<2I', 12, len(self._data))
		offset = 0
		_list = []
		for _str in self._data:
			enc_str = _str.encode('ascii')
			_list.append((getHash(enc_str), enc_str))
		_list.sort()
		for _hash, string in _list:
			res += pack('<3I', _hash, offset, len(string))
			offset += len(string) + 1
		res += pack('<I', offset)
		for _, string in _list:
			res += string + b'\x00'
		return res
