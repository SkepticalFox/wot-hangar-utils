""" BWCS (CompiledSpaceSettings) """

from struct import pack
from _base_json_section import *



class BWCS_Section(Base_JSON_Section):
	header = 'BWCS'
	int1 = 1

	def to_bin(self):
		return pack('<I6f', 24, *self._data)
