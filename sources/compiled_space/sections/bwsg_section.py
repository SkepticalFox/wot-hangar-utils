﻿""" BWSG (Static Geometry) """

from struct import pack
from _base_json_section import *



def packPositionInfo(item):
	return pack(
		'<5I',
		item['type'][0],
		item['type'][1],
		item['size'],
		item['chunk'],
		item['position']
	)



def packModelInfo(item):
	return pack(
		'<5I',
		item['key_name'],
		item['key_from'],
		item['key_to'],
		item['int4'],
		item['key_type']
	)



class BWSG_Section(Base_JSON_Section):
	header = 'BWSG'
	int1 = 2

	def to_bin(self):
		res = pack('<2I', 12, len(self._data['strings']))
		offset = 0
		for item in self._data['strings']:
			string = item['string'].encode('ascii')
			hash = getHash(string)
			assert hash == item['hash']
			res += pack('<3I', hash, offset, len(string))
			offset += len(string) + 1
		res += pack('<I', offset)
		for item in self._data['strings']:
			res += item['string'].encode('ascii') + b'\x00'
		res += self.write_entries(self._data['models'], 20, packModelInfo)
		res += self.write_entries(self._data['positions'], 20, packPositionInfo)
		res += self.write_entries(self._data['unknown_list'], 4, '<I')
		return res
