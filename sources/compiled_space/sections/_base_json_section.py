import json
import os

from struct import pack, calcsize
from _base_section import *



def getHash(data):
	"""
	Implementation of Fowler/Noll/Vo hash algorithm in pure Python.

	See http://isthe.com/chongo/tech/comp/fnv/
	"""
	hval = 0xcbf29ce484222325
	fnv_prime = 0x100000001b3
	fnv_size = 2**64
	for byte in data:
		hval = hval ^ ord(byte)
		hval = (hval * fnv_prime) % fnv_size
	return hval & 0xffffffff



def packBools(b1, b2, b3, b4):
	return int(b1) | (int(b2) << 8) | (int(b3) << 16) | (int(b4) << 24)



class Base_JSON_Section(Base_Section):
	def from_dir(self, unp_dir):
		json_file = os.path.join(unp_dir, '%s.json' % self.header)
		self._exist = os.path.exists(json_file)
		if not self._exist:
			return
		with open(json_file, 'r') as fr:
			self._data = json.load(fr)

	@staticmethod
	def write_entries(entries, ex_sz, arg):
		res = pack('<2I', ex_sz, len(entries))
		if isinstance(arg, str):
			sz = calcsize(arg)
			assert sz == ex_sz, (sz, ex_sz)
			for item in entries:
				if isinstance(item, (tuple, list)):
					res += pack(arg, *item)
				else:
					res += pack(arg, item)
		elif callable(arg):
			for item in entries:
				res += arg(item)
		else:
			assert False
		return res
