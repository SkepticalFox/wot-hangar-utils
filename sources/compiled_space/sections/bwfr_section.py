""" BWfr (Flare) """

from struct import pack
from _base_json_section import *



class BWfr_Section(Base_JSON_Section):
	header = 'BWfr'
	int1 = 2

	def to_bin(self):
		def packFlareInfo(item):
			return pack(
				'<I9f2I',
				item['resource_id'],
				item['max_distance'],
				item['area'],
				item['fade_speed'],
				item['position'][0],
				item['position'][1],
				item['position'][2],
				item['colour'][0],
				item['colour'][1],
				item['colour'][2],
				item['always_1'],
				item['visibility_mask']
			)
		return self.write_entries(self._data, 48, packFlareInfo)
