﻿""" BSMO (Static Model) """

from struct import pack
from _base_json_section import *



class BSMO_Section(Base_JSON_Section):
	header = 'BSMO'
	int1 = 1

	def to_bin(self):
		res = self.write_entries(self._data['01_08'], 8, '<2I')
		res += self.write_entries(self._data['02_36'], 36, '<6f3I')
		res += self.write_entries(self._data['03_08'], 8, '<2I')
		res += self.write_entries(self._data['04_24'], 24, '<6f')
		res += self.write_entries(self._data['05_08'], 8, '<2I')
		res += self.write_entries(self._data['06_04'], 4, '<I')
		res += self.write_entries(self._data['07_04'], 4, '<I')
		res += self.write_entries(self._data['08_08'], 8, '<2I')
		def pack_09_28_item(item):
			return pack(
				'<2i5I',
				item['node_id_from'],
				item['node_id_to'],
				item['index'],
				item['id_from_visual'],
				item['vertices_id'],
				item['indices_id'],
				int(item['treat_as_world_space_object'])
			)
		res += self.write_entries(self._data['09_28'], 28, pack_09_28_item)
		res += self.write_entries(self._data['10_04'], 4, '<I')
		def pack_11_36_item(item):
			return pack(
				'<If2i5I',
				item['animation_name_id'],
				item['frame_rate'],
				item['first_frame'],
				item['last_frame'],
				item['cognate_id'],
				item['animation_path_id'],
				item['6'],
				item['7'],
				item['anca_path_id']
			)
		res += self.write_entries(self._data['11_36'], 36, pack_11_36_item)
		res += self.write_entries(self._data['12_04'], 4, '<i')
		res += self.write_entries(self._data['13_68'], 68, '<i16f')
		res += self.write_entries(self._data['14_64'], 64, '<16I')
		res += self.write_entries(self._data['15_44'], 44, '<11I')
		res += self.write_entries(self._data['16_36'], 36, '<9I')
		res += self.write_entries(self._data['17_08'], 8, '<2I')
		return res
