﻿''' ShadowHunterRUS - 2017 '''

import ResMgr
import struct
from compiled_space.sections._base_json_section import getHash



def get_file_sha1(filepath):
	from hashlib import sha1
	with open(filepath, 'rb') as f:
		hexsha = sha1(f.read()).hexdigest()
	return hexsha



def get_vec16_from_mat4x4(mat):
	vec16 = []
	for i in range(0, 4):
		for j in range(0, 4):
			vec16.append(mat.get(i, j))
	return vec16



class ModHangarUtils:
	space_path = None
	hangar_name = None
	models = None
	c_space = None
	__strings = None

	def __init__(self, hangar_name):
		self.hangar_name = hangar_name
		self.space_path = ResMgr.resolveToAbsolutePath('spaces/%s' % self.hangar_name)
		try:
			self.create_space_bin()
			import gui.ClientHangarSpace as chs
			chs._SERVER_CMD_CHANGE_HANGAR = ''
			chs._SERVER_CMD_CHANGE_HANGAR_PREM = ''
			chs._getDefaultHangarPath = lambda isPremium: 'spaces/%s' % self.hangar_name
			from constants import IS_CHINA
			chs.readHangarSettings('igrPremHangarPath' + ('CN' if IS_CHINA else ''))
		except:
			import traceback
			traceback.print_exc()

	def sections_from_hangar_chunk(self):
		# hangar_chunk_sha1 = get_file_sha1(ResMgr.resolveToAbsolutePath('spaces/%s/hangar.chunk' % self.hangar_name))
		chunk = ResMgr.openSection('spaces/%s/hangar.chunk' % self.hangar_name)
		if chunk is None:
			return
		lights = []
		models = []
		particles = []
		list_audio = []
		for item in chunk.items():
			if item[0] in ('omniLight', 'spotLight', 'pulseLight', 'pulseSpotLight'):
				lights.append(item)
			elif item[0] == 'model':
				models.append(item[1])
			elif item[0] == 'particles':
				particles.append(item[1])
			elif item[0] == 'audio':
				list_audio.append(item[1])
		self.load_BWST()
		self.save_models(models)
		self.save_BWLC(lights)
		self.save_BWPs(particles)
		self.save_WTau(list_audio)
		self.save_BWST()

	def load_BWST(self):
		self.__strings = set(self.c_space.sections['BWST']._data)
		self.c_space.sections['BWST']._data = []

	def add_str(self, _str):
		self.__strings.add(_str)
		return getHash(_str.encode('ascii'))

	def save_BWST(self):
		self.c_space.sections['BWST']._data = list(self.__strings)

	def save_material(self, material):
		BSMA = self.c_space.sections['BSMA']._data
		key_from = len(BSMA['props'])
		key_fx = 0
		def get_type_by_prop(sec):
			if sec.has_key('Bool'):
				value = int(sec['Bool'].asBool)
				value_type = 1
			elif sec.has_key('Float'):
				value = sec['Float'].asFloat
				value_type = 2
			elif sec.has_key('Int'):
				value = sec['Int'].asInt
				value_type = 3
			elif sec.has_key('Vector4'):
				value = len(BSMA['vectors'])
				BSMA['vectors'].append(sec['Vector4'].asVector4)
				value_type = 5
			elif sec.has_key('Texture'):
				value = self.add_str(sec['Texture'].asString)
				value_type = 6
			else:
				assert False
			key = self.add_str(sec.asString)
			return {'key': key, 'value': value, 'value_type': value_type}

		for name, sect in material.items():
			if name == 'fx':
				_fx_hash = self.add_str(sect.asString)
				if _fx_hash not in BSMA['fx']:
					key_fx = len(BSMA['fx'])
					BSMA['fx'].append(_fx_hash)
				else:
					key_fx = BSMA['fx'].index(_fx_hash)
			elif name == 'property':
				BSMA['props'].append(get_type_by_prop(sect))
		key_to = len(BSMA['props']) - 1
		BSMA['materials'].append({
			'key_from': key_from,
			'key_fx': key_fx,
			'key_to': key_to
		})

	def save_models(self, models):
		i = 0
		j = 0
		start_pg = 0
		resources = {}
		for value in models:
			m_res = value.readString('resource')
			if m_res not in resources:
				model_sec = ResMgr.openSection(m_res)
				if model_sec is None:
					continue

				visual_path = model_sec.readString('nodefullVisual', model_sec.readString('nodelessVisual'))
				visual = ResMgr.openSection(visual_path + '.visual_processed')
				if visual is None:
					continue

				resources[m_res] = {
					'id': i
				}

				### Begin Animation
				# TODO
				resources[m_res]['animations'] = {}
				anim_id = 0
				for name, sect in model_sec.items():
					if name != 'animation':
						continue
					break # not working yet
					anim_name = sect.readString('name')
					animation_path = sect.readString('nodes') + '.animation'
					anim_frameRate = sect.readFloat('frameRate')
					anca_path = visual_path + '.anca'
					self.c_space.sections['BSMO']._data['11_36'].append({
						'animation_name_id': self.add_str(anim_name),
						'frame_rate': anim_frameRate,
						'first_frame': 0,
						'last_frame': -1,
						'cognate_id': 0,
						'animation_path_id': self.add_str(animation_path),
						'6': 0,
						'7': 19,
						'anca_path_id': self.add_str(anca_path)
					})
					resources[m_res]['animations'][anim_name] = anim_id
					anim_id += 1
				### End Animation

				primitives = visual.readString('primitivesName', visual_path) + '.primitives'
				bbmin = visual.readVector3('boundingBox/min')
				bbmax = visual.readVector3('boundingBox/max')
				self.c_space.sections['BSMO']._data['02_36'].append([
					bbmin[0], bbmin[1], bbmin[2],
					bbmax[0], bbmax[1], bbmax[2],
					self.add_str(primitives),
					i, i
				])
				self.c_space.sections['BSMO']._data['03_08'].append([0, 27776])
				vboxmin = model_sec.readVector3('visibilityBox/min', bbmin)
				vboxmax = model_sec.readVector3('visibilityBox/max', bbmax)
				self.c_space.sections['BSMO']._data['04_24'].append([
					vboxmin[0], vboxmin[1], vboxmin[2],
					vboxmax[0], vboxmax[1], vboxmax[2]
				])
				self.c_space.sections['BSMO']._data['05_08'].append([0, 4294967295])
				self.c_space.sections['BSMO']._data['06_04'].append(4294967295)
				if model_sec.has_key('extent'):
					m_extent = model_sec.readFloat('extent')
					res_extent = struct.unpack('<I', struct.pack('<f', m_extent**2))[0]
				else:
					res_extent = 2139095039 # 3.4028234663852886e+38
				self.c_space.sections['BSMO']._data['07_04'].append(res_extent)
				
				### nodes ###
				nodes_data = {}
				if visual['node'].has_key('node'):
					def pack_elem(elem, node_id, identifier):
						vec16 = get_vec16_from_mat4x4(elem.readMatrix('transform'))
						nodes_data[identifier] = len(self.c_space.sections['BSMO']._data['13_68'])
						self.c_space.sections['BSMO']._data['13_68'].append([
							node_id,
							vec16[0], vec16[1], vec16[2], vec16[3],
							vec16[4], vec16[5], vec16[6], vec16[7],
							vec16[8], vec16[9], vec16[10], vec16[11],
							vec16[12], vec16[13], vec16[14], vec16[15]
						])
					node_root = visual['node']
					pack_elem(node_root, -1, 'Scene Root')
					def rec_node(node):
						node_id = 0
						for elem_name, elem in node.items():
							if elem_name != 'node':
								continue
							node_id += 1
							identifier = elem.readString('identifier')
							pack_elem(elem, node_id, identifier)
							node_id += rec_node(elem)
						return node_id
					rec_node(node_root)
				### nodes ###
				
				for curName, curSect in visual.items():
					if curName != 'renderSet':
						continue
					primitives_v = primitives + '/' + curSect.readString('geometry/vertices')
					primitives_i = primitives + '/' + curSect.readString('geometry/primitive')
					treatAsWorldSpaceObject = curSect.readBool('treatAsWorldSpaceObject', False)
					rs_nodes = curSect.readStrings('node')
					if len(rs_nodes) == 1 and rs_nodes[0] == 'Scene Root':
						node_id_from = -1
						node_id_to = -1
					else:
						node_id_from = len(self.c_space.sections['BSMO']._data['10_04'])
						for _node in rs_nodes:
							self.c_space.sections['BSMO']._data['10_04'].append(nodes_data[_node])
						node_id_to = len(self.c_space.sections['BSMO']._data['10_04']) - 1
					for curSubName, curSubSect in curSect['geometry'].items():
						if curSubName != 'primitiveGroup':
							continue
						self.save_material(curSubSect['material'])
						# collisionFlags = curSubSect.readInt('collisionFlags', 0)
						# materialKind = curSubSect.readInt('materialKind', 0)
						self.c_space.sections['BSMO']._data['09_28'].append({
							'node_id_from' : node_id_from,
							'node_id_to' : node_id_to,
							'index' : j, # maybe index BSMO/05_08
							'id_from_visual' : curSubSect.asInt,
							'vertices_id' : self.add_str(primitives_v),
							'indices_id' : self.add_str(primitives_i),
							'treat_as_world_space_object' : treatAsWorldSpaceObject
							})
						j += 1
				self.c_space.sections['BSMO']._data['01_08'].append([i, i])
				self.c_space.sections['BSMO']._data['08_08'].append([start_pg, j-1])
				start_pg = j
				i += 1
			mat4x4 = value.readMatrix('transform')
			m_transform = get_vec16_from_mat4x4(mat4x4)
			self.c_space.sections['BSMI']._data['01_64'].append(m_transform)
			m_castShadows = value.readBool('castShadows', False)
			m_reflectionVisible = value.readBool('reflectionVisible', False)
			m_castsLocalShadow = value.readBool('castsLocalShadow', False)
			m_shadowProxy = value.readBool('shadowProxy', False)
			not_ignores_objects_farplane = not value.readBool('ignoresObjectsFarplane', False)
			self.c_space.sections['BSMI']._data['02_08'].append({
				'casts_shadow': m_castShadows,
				'reflection_visible': m_reflectionVisible,
				'shadow_proxy': m_shadowProxy,
				'casts_local_shadow': m_castsLocalShadow,
				'not_ignores_objects_farplane': not_ignores_objects_farplane,
				'has_animations': bool(resources[m_res]['animations']),
				'0': 0,
				'1': 1
			})
			if resources[m_res]['animations']:
				animation_id = -1
				frame_rate_multiplier = 1.0
				if value.has_key('animation'):
					anim_name = value.readString('animation/name', '')
					animation_id = resources[m_res]['animations'][anim_name]
					frame_rate_multiplier = value.readFloat('animation/frameRateMultiplier', 1.0)
				self.c_space.sections['BSMI']._data['06_16'].append({
					'unknown_1': 0,
					'animation_id': animation_id,
					'unknown_2': 0,
					'frame_rate_multiplier': frame_rate_multiplier
				})
				self.c_space.sections['BSMI']._data['03_04'].append(4294967295)
				self.c_space.sections['BSMI']._data['04_04'].append(resources[m_res]['id'])
				self.c_space.sections['BSMI']._data['05_04'].append(4294967295)
				self.c_space.sections['BSMI']._data['07_04'] = [				
					0,
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9,
					10,
					11,
					12,
					13,
					14,
					15,
					16,
					17,
					18,
					19,
					20,
					21,
					22,
					23,
					24,
					25,
					26,
					27,
					28,
					29,
					30,
					31,
					32,
					33,
					34,
					35,
					36,
					37,
					38,
					39,
					40,
					41,
					42,
					43,
					44,
					45,
					46,
					47,
					48,
					49,
					50,
					51,
					52,
					53,
					54,
					55,
					56,
					57,
					58,
					59,
					60,
					61,
					62,
					63,
					64,
					65,
					66,
					67,
					68,
					69,
					70,
					71,
					72,
					73,
					74,
					75,
					76,
					77,
					78,
					79,
					80,
					81,
					82,
					83,
					84,
					85,
					86,
					87,
					88,
					89,
					90,
					91,
					92,
					93,
					94,
					95,
					96,
					97,
					98,
					99,
					100,
					101
				]
			else:
				self.c_space.sections['BSMI']._data['03_04'].append(4294967295)
				self.c_space.sections['BSMI']._data['04_04'].append(resources[m_res]['id'])
				self.c_space.sections['BSMI']._data['05_04'].append(4294967295)

	def save_WTau(self, list_audio):
		for value in list_audio:
			wwevent_name = value.readString('wweventName', '')
			event_name = value.readString('eventName', '')
			if value.has_key('transform'):
				position = value.readMatrix('transform').translate
			else:
				position = value.readVector3('position')
			self.c_space.sections['WTau']._data['1'].append({
				'wwevent_name_id': self.add_str(wwevent_name),
				'event_name_id': self.add_str(event_name),
				'max_distance': value.readFloat('maxDistance'),
				'transform': position
			})

	def save_BWPs(self, particles):
		for value in particles:
			res = value.readString('resource')
			res_sec = ResMgr.openSection(res)
			seedTime = 0.1
			if res_sec is not None:
				seedTime = res_sec.readFloat('seedTime', 0.1)
			self.c_space.sections['BWPs']._data.append({
				'transform': get_vec16_from_mat4x4(value.readMatrix('transform')),
				'resource_id': self.add_str(res),
				'reflection_visible': value.readBool('reflectionVisible'),
				'seed_time': seedTime
			})

	def save_BWLC(self, lights):
		frames = self.c_space.sections['BWLC']._data['frames']
		frame_start_id = len(frames)
		for key, value in lights:
			if key == 'omniLight':
				omni_light = {
					'position': value.readVector3('position'),
					'inner_radius': value.readFloat('innerRadius'),
					'outer_radius': value.readFloat('outerRadius'),
					'colour': value.readVector3('colour'),
					'unknown_1': 1.0,
					'unknown_2': 0,
					'multiplier': value.readFloat('multiplier'),
					'cast_shadows': value.readBool('castShadows', False),
				}
				self.c_space.sections['BWLC']._data['omni_light_list'].append(omni_light)
			elif key == 'spotLight':
				spot_light = {
					'position': value.readVector3('position'),
					'direction': value.readVector3('direction'),
					'inner_radius': value.readFloat('innerRadius'),
					'outer_radius': value.readFloat('outerRadius'),
					'cone_angle': value.readFloat('angle'),
					'colour': value.readVector3('colour'),
					'unknown_1': 1.0,
					'unknown_2': 0,
					'multiplier': value.readFloat('multiplier'),
					'cast_shadows': value.readBool('castShadows', False)
				}
				self.c_space.sections['BWLC']._data['spot_light_list'].append(spot_light)
			elif key == 'pulseLight':
				frame_num = 0
				for vec2 in value.readVector2s('frame'):
					frame_num += 1
					frames.append(list(vec2))
				pulse_light = {
					'position': value.readVector3('position'),
					'inner_radius': value.readFloat('innerRadius'),
					'outer_radius': value.readFloat('outerRadius'),
					'colour': value.readVector3('colour'),
					'unknown_1': 1.0,
					'unknown_2': 0,
					'multiplier': value.readFloat('multiplier'),
					'cast_shadows': value.readBool('castShadows', False),
					'animation_id': self.add_str(value.readString('animation', '')),
					'frame_start_id': frame_start_id,
					'frame_num': frame_num,
					'duration': value.readFloat('duration')
				}
				frame_start_id += frame_num
				self.c_space.sections['BWLC']._data['pulse_light_list'].append(pulse_light)
			elif key == 'pulseSpotLight':
				frame_num = 0
				for vec2 in value.readVector2s('frame'):
					frame_num += 1
					frames.append(list(vec2))
				pulse_spot_light = {
					'position': value.readVector3('position'),
					'direction': value.readVector3('direction'),
					'inner_radius': value.readFloat('innerRadius'),
					'outer_radius': value.readFloat('outerRadius'),
					'cone_angle': value.readFloat('angle'),
					'colour': value.readVector3('colour'),
					'unknown_1': 1.0,
					'unknown_2': 0,
					'multiplier': value.readFloat('multiplier'),
					'unknown_3': 0,
					'cast_shadows': value.readBool('castShadows', False),
					'animation_id': self.add_str(value.readString('animation', '')),
					'frame_start_id': frame_start_id,
					'frame_num': frame_num,
					'duration': value.readFloat('duration')
				}
				frame_start_id += frame_num
				self.c_space.sections['BWLC']._data['pulse_spot_light_list'].append(pulse_spot_light)
		self.c_space.sections['BWLC']._data['frames'] = frames

	def create_space_bin(self):
		from compiled_space import CompiledSpace
		self.c_space = CompiledSpace()
		self.c_space.from_dir('%s/space_bin_kitchen' % self.space_path)
		self.sections_from_hangar_chunk()
		self.c_space.save_to_bin('%s/space.bin' % self.space_path)



def init():
    global generate_space_bin
    global current_space

    current_xml = ResMgr.openSection('spaces/current.xml')
    if current_xml is None:
        return

    current_space = current_xml.readString('space', '')
    if not current_space:
        return
    generate_space_bin = current_xml.readBool('generate_space_bin', True)

    ModHangarUtils(current_space)
    refresh_space_hotkey = current_xml.readString('refresh_space_hotkey', '')
    if not refresh_space_hotkey:
        return

    from gui.shared.utils.key_mapping import getBigworldNameFromKey
    from gui.shared.utils.HangarSpace import _HangarSpace, g_hangarSpace

    def onhandleKeyEvent(event):
        key = getBigworldNameFromKey(event.key)
        if key == refresh_space_hotkey:
            try:
                isPremium = g_hangarSpace.isPremium
                g_hangarSpace.destroy()
                ModHangarUtils(current_space)
                g_hangarSpace.init(isPremium)
            except:
                import traceback
                traceback.print_exc()

    from gui import InputHandler
    InputHandler.g_instance.onKeyDown += onhandleKeyEvent
